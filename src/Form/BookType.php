<?php

namespace App\Form;

use App\Entity\Book;
use App\Entity\Option;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class BookType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('description')
            ->add('author')
            ->add('editor')
            ->add('imageFile',FileType::class,[
                "required"=>false
            ])
            ->add('options', EntityType::class ,
            [
                'class'=> Option::class,
                'choice_label'=> 'name',
                'multiple'=> true
            ])
            
            ->add('sold')
            ->add('isbn')
            ->add('ean')
           
            ->add('price')
            ->add('category', ChoiceType::class,[
                'choices'=> $this->getChoices()
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Book::class,
            'translation_domain' =>'forms'
        ]);
    }

    public function getChoices()
    {
        $choices= Book::CATEGORY;
        $output = [];
        foreach($choices as $k => $v){
            $output[$v]=$k;


        }
        return $output;
    }
}
