<?php 


namespace App\Form;

use App\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;





class ContactType extends AbstractType 
{

    public function buildForm(FormBuilderInterface $builder, array $option)
    {
        $builder
        
        ->add('message',TextareaType::class)

        ;
    }
    


}